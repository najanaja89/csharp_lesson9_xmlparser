﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CSharp_Lesson9_XmlParser
{
    class Program
    {
        static void Main(string[] args)
        {

            #region write;
            //List<Car> cars = new List<Car>
            //{
            //    new Car
            //    {
            //        Mark = "Toyota",
            //        Model = "LC 200",
            //        Color = "Black",
            //        Price = 60000
            //    },

            //    new Car
            //    {
            //        Mark = "BMW",
            //        Model = "5",
            //        Color = "Silver",
            //        Price = 58000
            //    },

            //    new Car
            //    {
            //        Mark = "Hyundai",
            //        Model = "Accent",
            //        Color = "Black",
            //        Price = 22000
            //    }
            //};


            //XmlDocument xmlDocument = new XmlDocument();
            //XmlElement rootElement = xmlDocument.CreateElement("cars");

            //foreach (var car in cars)
            //{
            //    XmlElement carElement = xmlDocument.CreateElement("car");
            //    carElement.SetAttribute("Mark", car.Mark);
            //    carElement.SetAttribute("Model", car.Model);
            //    carElement.SetAttribute("Color", car.Color);
            //    carElement.SetAttribute("Price", car.Price.ToString());

            //    rootElement.AppendChild(carElement);
            //}

            //xmlDocument.AppendChild(rootElement);
            //xmlDocument.Save("cars.xml");
            #endregion

            #region read
            List<Car> cars = new List<Car>();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("cars.xml");

            XmlElement rootElement = xmlDocument.DocumentElement;
            foreach (XmlElement element in rootElement.ChildNodes)
            {
                string mark = element.Attributes["Mark"].Value;
                string model = element.Attributes["Model"].Value;
                string color = element.Attributes["Color"].Value;
                double price = double.Parse(element.Attributes["Price"].Value);

                cars.Add(
                  new Car
                  {
                      Mark = mark,
                      Model = model,
                      Color = color,
                      Price = price
                  }
               );
            }
            #endregion
            Console.ReadLine();
        }
    }
}
